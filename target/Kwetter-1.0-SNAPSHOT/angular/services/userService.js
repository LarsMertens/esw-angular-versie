angular.module('userService', [])

    .factory('User', function($http) {

        var model = 'Kwetter/api/user';

        return {
            getLoggedInUser : function(){
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getLoggedInUser'
                });
            },
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getAll'
                });
            },
            getAllFollowing : function($id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getAllFollowing/'+$id+''
                });
            },
            getAllFollowers : function($id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getAllFollowers/'+$id+''
                });
            },
            getLatestTweets : function($id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getLatestTweets/'+$id+''
                });
            },
            addFollowing : function($my_id, $following_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/addFollowing/'+$my_id+'/'+$following_id+''
                    //data: $object
                });
            },
            removeFollowing : function($my_id, $following_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/removeFollowing/'+$my_id+'/'+$following_id+''
                    //data: $object
                });
            },
            addFollower : function($my_id, $following_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/addFollower/'+$my_id+'/'+$following_id+''
                    //data: $object
                });
            },
            removeFollower : function($my_id, $following_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/removeFollower/'+$my_id+'/'+$following_id+''
                    //data: $object
                });
            },
            countFollowing : function($my_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/countFollowing/'+$my_id+''
                    //data: $object
                });
            },
            countFollowers : function($my_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/countFollowers/'+$my_id+''
                    //data: $object
                });
            },
            isFollowing : function($my_id, $following_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/isFollowing/'+$my_id+'/'+$following_id+''
                });
            },
            getUserByTweetID : function($tweet_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getUserByTweetID/'+$tweet_id+''
                });
            },
            searchUsers : function($search_value) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/search/'+$search_value+''
                });
            }
        };
    });