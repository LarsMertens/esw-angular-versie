angular.module('tweetService', [])

    .factory('Tweet', function($http) {

        var model = 'Kwetter/api/tweet';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getAll'
                });
            },
            countMyTweets : function($user_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/countMyTweets/'+$user_id+''
                });
            },
            getMentions : function($user_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getMentions/'+$user_id+''
                });
            },
            getTimeline : function($user_id) {
                return $http({
                    method: 'GET',
                    cache: false,
                    url: '/'+model+'/getTimeline/'+$user_id+''
                });
            },
            isFavoriteTweet : function($user_id, $tweet_id) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/isFavoriteTweet/'+$user_id+'/'+$tweet_id+''
                });
            },
            addTweet : function($object, $user_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/addTweet/'+$user_id+'',
                    data: $object
                });
            },
            addFavoriteTweet : function($user_id, $tweet_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/addFavoriteTweet/'+$user_id+'/'+$tweet_id+'',
                });
            },
            removeFavoriteTweet : function($user_id, $tweet_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/removeFavoriteTweet/'+$user_id+'/'+$tweet_id+'',
                });
            },
            search : function($search_value) {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/search/'+$search_value+''
                    //data: $object
                });
            }
        };
    });