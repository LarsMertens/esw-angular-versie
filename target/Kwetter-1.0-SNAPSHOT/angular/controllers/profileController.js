angular.module('profileCtrl', [])

    .controller('profileController', function($scope, $http, $window, $timeout, Profile, User) {

        $scope.loading = false;
        $scope.isSaving = false;
        $scope.profile = {};

        $scope.init = function($id) {
            User.getLoggedInUser()
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get the data of a single object
         *
         * @param {type} $id
         * @returns {undefined}
         */
        $scope.get = function($id){
            Profile.get($id)
                .then(function (success) {
                    $scope.profile = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        /**
         * Get the data of all objects in database
         */
        $scope.getAll = function(){
            Profile.getAll()
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.upload = function(){
            $scope.profile.avatarFile = $scope.profile.avatar[0];
            $scope.profile.imageFile = $scope.profile.image[0];
            console.log($scope.profile);
            Profile.upload($scope.profile, $scope.object.id)
                .then(function (success){
                    console.log("profile aangemaakt");
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
        };
    });





