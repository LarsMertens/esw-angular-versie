//http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/HomeWebsocket/WebsocketHome.html#section4 -> mogelijke implementatie om items op frontpage te generate

var wsUri = "ws://localhost:8080/Kwetter/websocket";

//connect websocket
function connect() {

    output = document.getElementById("output");

    websocket = new WebSocket(wsUri);
    console.log("OPEN");

    websocket.onopen = function (evt) {
        onOpen(evt);
        console.log("OPEN");
    };
    websocket.onclose = function (evt) {
        onClose(evt);
        console.log("Close");

    };
    websocket.onmessage = function (evt) {
        onMessage(evt);
        console.log("Message");
    };
    websocket.onerror = function (evt) {
        onError(evt);
        console.log("Error");
    };
}

//define event handlers
function onOpen(evt) {
    writeToScreen("CONNECTED");
}

function onClose(evt) {
    writeToScreen("DISCONNECTED");
}
function onMessage(evt) {
    writeToScreen("MESSAGE: " + evt.data);
    //console.log(evt.data);
  var dataObject = JSON.parse(evt.data);
  console.log(dataObject);
  
  var parent = document.getElementById('tabView:theForm');
  var newChild = 
          ' <div class="tweet-block col-xs-12 no-padding">'+
    '<div class="col-xs-2">'+
        '<a href="user.xhtml?userID='+dataObject.user.id+'" class="avatar-big" style="background-image: url(\'uploads/'+dataObject.profile.avatar+'\')"></a>'+
    '</div>'+
    '<div class="col-xs-10">'+
        '<div class="userthings col-xs-12 no-padding">'+
            '<b>'+dataObject.user.firstname + " " + dataObject.user.lastname+'</b> '+
            '<span>@'+dataObject.user.username+'</span>'+
            '<span class="times">'+dataObject.tweet.created_at+''+
            '</span>'+
        '</div>'+
        '<div class="clearfix"></div>'+
        '<div class="tweetinsidecontent col-xs-12 no-padding">'+dataObject.tweet.content+''+
            '<br><br>'+
                        '<small>'+
            '<span onclick="myremote('+dataObject.tweet.id+');" class="fa fa-heart-o"></span></a>'+       
    '</small>'+
        '</div>'+
   ' </div>' +
'</div>';
  parent.insertAdjacentHTML('afterbegin', newChild);
  

}

function onError(evt) {
    writeToScreen("ERROR");
}

function writeToScreen(text) {
    var pre = document.createElement("p");
    pre.style.wordWrap = "break-word";
    pre.innerHTML = text;
    output.appendChild(pre);
}


function writeToStudentTable(text) {
    var table = document.getElementById("studtable");
    var row = table.insertRow(1);
    var cellname = row.insertCell(0);
    var cellage = row.insertCell(1);
    var cellskill = row.insertCell(2);
    cellname.innerHTML = text;
    cellage.innerHTML = "20";
    cellskill.innerHTML = "SpringBoot";
}

