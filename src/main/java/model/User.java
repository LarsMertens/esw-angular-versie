package model;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
    @NamedQuery(name = "user.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
    @NamedQuery(name = "user.findByUsername", query = "SELECT u FROM User u WHERE u.username LIKE :username"),
    @NamedQuery(name = "user.getByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
})

@XmlRootElement
public class User implements Serializable {
    
    @Id @GeneratedValue
    private Long id;
    private String username;
    private String email;
    private String password;
    private String type;
    private String firstname;
    private String lastname;
    private Date date_of_birth;
    private Date created_at;
    private Date updated_at;
    
    @JoinTable(name = "USERS_GROUPS",
            joinColumns
            = @JoinColumn(name = "USERNAME", referencedColumnName = "username"),
            inverseJoinColumns
            = @JoinColumn(name = "GROUPNAME", referencedColumnName = "groupName")
    )
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Collection<Group> group = new ArrayList<>();
    
    @OneToOne(mappedBy = "user")
    private Profile profile;
    
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable
    (
        name="USER_TWEET",
        joinColumns={ @JoinColumn(name="ID", referencedColumnName="ID") },
        inverseJoinColumns={ @JoinColumn(name="TWEET_ID", referencedColumnName="ID") }
    )
    private List<Tweet> tweets;
    
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable
    (
        name="USER_FAVORITES",
        joinColumns={ @JoinColumn(name="ID", referencedColumnName="ID") },
        inverseJoinColumns={ @JoinColumn(name="USER_FAVORITED_ID", referencedColumnName="ID") }
    )
    private List<Tweet> favorites;
    
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable
    (
        name="USER_MENTIONS",
        joinColumns={ @JoinColumn(name="ID", referencedColumnName="ID") },
        inverseJoinColumns={ @JoinColumn(name="TWEET_ID", referencedColumnName="ID") }
    )
    private List<Tweet> mentions;
    
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @XmlTransient
    @JoinTable
    (
        name="USER_FOLLOWING",
        joinColumns={ @JoinColumn(name="ID", referencedColumnName="ID") },
        inverseJoinColumns={ @JoinColumn(name="USER_FOLLOWING_ID", referencedColumnName="ID") }
    )
    private List<User> following;
    
    @ManyToMany(mappedBy = "following", fetch = FetchType.EAGER)
    @XmlTransient
    private List<User> followingUsers;
    
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @XmlTransient
    @JoinTable
    (
        name="USER_FOLLOWERS",
        joinColumns={ @JoinColumn(name="ID", referencedColumnName="ID") },
        inverseJoinColumns={ @JoinColumn(name="USER_FOLLOWERS_ID", referencedColumnName="id") }
    )
    private List<User> followers;
    
    @ManyToMany(mappedBy = "followers", fetch = FetchType.EAGER)
    @XmlTransient
    private List<User> followerUsers;

    public User(){}
    
    public List<Tweet> getMentions() {
        return mentions;
    }

    public void setMentions(List<Tweet> mentions) {
        this.mentions = mentions;
    }

    public User(String username, String email, String password, String type, String firstname, String lastname, Date date_of_birth) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.type = type;
        this.firstname = firstname;
        this.lastname = lastname;
        this.date_of_birth = date_of_birth;
        this.created_at = new Date();
        this.updated_at = new Date();
        this.tweets = new ArrayList<Tweet>();
        this.favorites = new ArrayList<Tweet>();
        this.mentions = new ArrayList<Tweet>();
        this.following = new ArrayList<User>();
        this.followers = new ArrayList<User>();
    }

    /**
     * Login the user if given email and password matches input values for email and @password
     * 
     * @param email : email format 
     * @param password : password for email
     * @return true if match - false if not
     */
    public boolean Login(String email, String password){
        return this.email.equals(email) && this.password.equals(password);
    }
    
    /**
     * Logs the user out
     * @return true
     */
    public boolean Logout(){
        return true;
    }

    /**
     * Returns a list of maximum 10 tweets if less than 10 return all of the remaining tweets
     * 
     * @return list of tweets - maximum 10 tweets else if less than 10 tweets return current 
     * number of tweets
     */
    public List<Tweet> getLatestTweets(){
        if(this.tweets.size() > 10){
            return this.tweets.subList(0, 10);
        } 
        return this.tweets;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Tweet> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<Tweet> favorites) {
        this.favorites = favorites;
    }
    
    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    @XmlTransient
    public List<User> getFollowing() {
        return following;
    }

    @XmlTransient
    public void setFollowing(List<User> following) {
        this.following = following;
    }

    @XmlTransient
    public List<User> getFollowers() {
        return followers;
    }

    @XmlTransient
    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }
    
    @XmlTransient
    public List<User> getFollowingUsers() {
        return followingUsers;
    }

    @XmlTransient
    public void setFollowingUsers(List<User> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public Collection<Group> getGroup() {
        return group;
    }

    public void setGroup(Collection<Group> group) {
        this.group = group;
    }

    @XmlTransient
    public List<User> getFollowerUsers() {
        return followerUsers;
    }

    @XmlTransient
    public void setFollowerUsers(List<User> followerUsers) {
        this.followerUsers = followerUsers;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    
}
