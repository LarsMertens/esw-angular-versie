package model;

import com.google.gson.annotations.Expose;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@NamedQueries({
    @NamedQuery(name = "profile.findById", query = "SELECT p FROM Profile p WHERE p.id = :id")
})


@XmlRootElement
public class Profile implements Serializable {
    
    @Id @GeneratedValue
    private Long id;
    private String biography;
    private String location;
    private String website;
    private String avatar;
    private String image;
    private Date created_at;
    private Date updated_at;
    
    @OneToOne
    @JoinColumn(name = "user_id")
    @XmlTransient
    private User user;

    public Profile(){}
    
    public Profile(String biography, String location, String website, String avatar, String image) {
        this.biography = biography;
        this.location = location;
        this.website = website;
        this.avatar = avatar;
        this.image = image;
        this.created_at = new Date();
        this.updated_at = new Date();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @XmlTransient
    public User getUser() {
        return user;
    }

    @XmlTransient
    public void setUser(User user) {
        this.user = user;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        if(biography.length() < 160){
            this.biography = biography;
        } else {
            throw new IllegalArgumentException("Biography got too many characters.");
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }
}
