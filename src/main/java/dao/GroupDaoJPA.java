package dao;

import model.Group;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class GroupDaoJPA extends DaoFacade<Group>  {

    @PersistenceContext(unitName = "KwetterPU")
    private EntityManager em;

    public GroupDaoJPA() {
        super(Group.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
