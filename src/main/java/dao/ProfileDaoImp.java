package dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import model.Profile;

@Stateless @Default
public class ProfileDaoImp implements ProfileDao {
    
    private HashMap<Long, Profile> profiles;
    private Long nextId;
    
    /**
     * This class will be used as data access layer and to create 
     * profile objects in memory
     */
    public ProfileDaoImp(){
        this.nextId = 1L;
        this.profiles = new HashMap<Long, Profile>();
        initProfiles();
    }
    
    /**
     * Initialise an in memory Hashmap of 10 profile objects
     */
    public void initProfiles(){
        for(int i = 1; i < 11; i++){
           Long id = new Long(i); 
           Profile profile = new Profile("biography" + id.toString(), "location" + id.toString(), 
                                         "website" + id.toString(), "../avatar.jpg" + id.toString(), 
                                         "../image.jpg" + id.toString());
           this.create(profile);
           id++;
        }
    }

    /**
     * Creates a profile object
     * if no profile is selected throw an IllegalArgumentException
     * @param p = the selected profile object
     */
    @Override
    public void create(Profile p) {
        if (p == null) {
            throw new IllegalArgumentException("Profile is null");
        }
        p.setId(this.nextId);
        this.profiles.put(this.nextId, p);
        this.nextId++;
    }
    
    /**
     * Get all profiles from a hashmap
     * @return all profiles as an ArrayList
     */
    @Override
    public List<Profile> findAll() {
       return new ArrayList(this.profiles.values()); 
    }

    /**
     * Get a single profile object from a hashmap
     * If a profile is not found by given id
     * @param id - id of the selected profile
     * @return a single profile object
     */
    @Override
    public Profile find(Long id) {
        if (!this.profiles.containsKey(id)) {
            throw new IllegalArgumentException("Id no found" + id);
        }
        return this.profiles.get(id);
    }

    /**
     * Updates a profile
     * @param transientProfile - updated profile
     * @return updated profile
     */
    @Override
    public Profile update(Profile transientProfile) {
        return null;
    }
}
