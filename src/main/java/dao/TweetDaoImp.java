package dao;

import Event.TweetEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.TypedQuery;
import model.Tweet;
import model.User;

@Stateless @Default
public class TweetDaoImp implements TweetDao {
    
    private HashMap<Long, Tweet> tweets;
    private Long nextId;
    //private UserDao userDao;

    /**
     * This class will be used as data access layer and to create 
     * tweet objects in memory
     */
    public TweetDaoImp(){
        this.nextId = 1L;
        this.tweets = new HashMap<Long, Tweet>();
        //this.userDao = new UserDaoImp();
        initTweets();
    }
    
    /**
     * Initialise an in memory Hashmap of 10 tweet objects
     */
    public void initTweets(){
        for(int i = 1; i < 11; i++){
           Long id = new Long(i); 
           Tweet tweet = new Tweet(1L, "subject" + id.toString(), "some text as content 1" + id.toString());
           tweet.setTrent(true);
           this.create(tweet, 1L);
           id++;
        }
    }

    /**
     * Creates a tweet object
     * if no tweet is selected throw an IllegalArgumentException
     * @param t = the selected tweet object
     * @return true or false
     */
    @Override
    public boolean create(Tweet t, Long id) {
        if (t == null) {
            throw new IllegalArgumentException("Tweet is null");
        }
        t.setId(this.nextId);
        this.tweets.put(this.nextId, t);
        this.nextId++;
        return true;
    }

    /**
     * Get all tweets from a hashmap
     * @return all tweets as an ArrayList
     */
    @Override
    public List<Tweet> findAll() {
       return new ArrayList(tweets.values()); 
    }

    /**
     * Get a single tweet object from a hashmap
     * If a tweet is not found by given id
     * @param id - id of the selected tweet
     * @return a single tweet object
     */
    @Override
    public Tweet find(Long id) {
        if (!tweets.containsKey(id)) {
            throw new IllegalArgumentException("Id no found" + id);
        }
        return tweets.get(id);
    }
    
    /**
     * Search for tweets by content
     * @param input - content tweet must contain
     * @return list of tweets
     */
    @Override
    public List<Tweet> search(String input) {
        ArrayList<Tweet> foundTweets = new ArrayList<Tweet>();
        for(int i = 1; i <= this.tweets.size(); i++){
            if(this.find(new Long(i)).getContent().contains(input)){
                foundTweets.add(this.find(new Long(i)));
            }
        }
        return foundTweets;
    }

    /**
     * Gets a list of all tweets that are trends
     * @return list of tweets that are trends
     */
    @Override
    public List<Tweet> getTrends() {
        ArrayList<Tweet> foundTweets = new ArrayList<Tweet>();
        for(int i = 1; i <= this.tweets.size(); i++){
            if(this.find(new Long(i)).isTrent()){
                foundTweets.add(this.find(new Long(i)));
            }
        }
        return foundTweets;
    }
    
    
    @Override
    public boolean create(TweetEvent tweet) {
        return true;
    }

    /**
     * Deletes an tweet
     * @param id - tweet id
     */
    @Override
    public void deleteTweet(Long id) {
        this.findAll().remove(this.find(id));
    }
    
    @Override
    public boolean isFavoriteTweet(Long user_id, Long tweet_id) {
        return true;
    }
    
    @Override
    public void addFavoriteTweet(Long user_id, Long tweet_id) {
        //userDao.find(user_id).getFavorites().add(this.find(tweet_id));
    }

    @Override
    public void removeFavoriteTweet(Long user_id, Long tweet_id) {
        //userDao.find(user_id).getFavorites().remove(this.find(tweet_id));
    }

    @Override
    public List<Tweet> getFavoriteTweets(Long user_id) {
        return null;
    }
}
