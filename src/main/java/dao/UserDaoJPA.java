package dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import model.Profile;
import model.Tweet;
import model.User;

@Stateless
@JPA
public class UserDaoJPA implements UserDao {

    @PersistenceContext(unitName = "KwetterPU")
    private EntityManager em;
    
    @Override
    public void create(User u) {
        em.persist(u);
    }

    @Override
    public List<User> findAll() {
        Query query = em.createQuery("SELECT u FROM User u");
        return new ArrayList<>(query.getResultList());
    }

    @Override
    public User find(Long id) {
        return em.find(User.class, id);
    }

    @Override
    public List<User> search(String input) {
        TypedQuery<User> query = em.createNamedQuery("user.findByUsername", User.class);
        query.setParameter("username", "%" + input + "%");
        List<User> result = query.getResultList();
        System.out.println("count: " + result.size());
        return result;
    }

    @Override
    public List<Tweet> getTimeline(Long id) {
        List<Tweet> timelineTweets = this.find(id).getTweets();
        Collections.sort(timelineTweets, Collections.reverseOrder());
        if(timelineTweets.size() >= 20){
            return timelineTweets.subList(0, 20);
        }
        return timelineTweets;
    }

    /**
     * Returns a list of the 10 latest tweets from the user
     * @param id - id from user
     * @return list of tweets latest 10
     */
    @Override
    public List<Tweet> getLatestTweets(Long id) {
        Query query = em.createQuery("select t " + 
                                     "from User u " +
                                     "join u.tweets t " +
                                     "where u.id = ?1 " +
                                     "order by t.created_at DESC");
        query.setParameter( 1, id ); 
        query.setMaxResults(10);
        return new ArrayList<Tweet>( query.getResultList() );
    }
    
    /**
     * Gets a list of all followers by user id
     * @param id - id from the user
     * @return all followers from the given user
     */
    @Override
    public List<User> getFollowers(Long id) {
        return this.find(id).getFollowers();
    }
    
    /**
     * Adds an follower to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be followed
     */
    @Override
    public void addFollower(Long my_id, Long user_id) {
        this.find(my_id).getFollowers().add(this.find(user_id));
    }
    
    /**
     * Removes follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    @Override
    public void removeFollower(Long my_id, Long user_id){
        this.find(my_id).getFollowers().remove(this.find(user_id));
    }
    
    /**
     * Gets a list of all following by user id
     * @param id - id from the user
     * @return all following from the given user
     */
    @Override
    public List<User> getFollowing(Long id) {
        return this.find(id).getFollowing();
    }
    
    /**
     * Adds an following to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be following
     */
    @Override
    public void addFollowing(Long my_id, Long user_id) {
        this.find(my_id).getFollowing().add(this.find(user_id));
    }
    
    /**
     * Removes following by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a following
     */
    @Override
    public void removeFollowing(Long my_id, Long user_id){
        this.find(my_id).getFollowing().remove(this.find(user_id));
    }

    /**
     * Get all mentions from an user
     * @param id - an user id
     * @return all mentions from an user 
     */
    @Override
    public List<Tweet> getMentions(Long id) {
        return this.find(id).getMentions();
    }

    @Override
    public List<Tweet> getFollowingTweets(Long id) {
        Query query = em.createQuery("select t " + 
                                     "from User u " +
                                     "join u.following t " +
                                     "where u.id = ?1 ");
        query.setParameter( 1, id ); 
        return new ArrayList<Tweet>( query.getResultList() );
    }
    
    @Override
    public boolean isFollowing(Long my_id, Long following_id) {
        Query query = em.createQuery("select t " + 
                                     "from User u " +
                                     "join u.following t " +
                                     "where u.id = ?1 " +   
                                     "and t.id = ?2 ");
        query.setParameter( 1, my_id ); 
        query.setParameter( 2, following_id );
        return query.getResultList().isEmpty();
    }

    @Override
    public User getUserByName(String username) {
        TypedQuery<User> query = em.createNamedQuery("user.getByUsername", User.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    @Override
    public void addMention(Long user_id, Long tweet_id) {
        TypedQuery<Tweet> query = em.createNamedQuery("tweet.findById", Tweet.class);
        query.setParameter("id", tweet_id);
        Tweet tweet = query.getSingleResult();
        this.find(user_id).getMentions().add(tweet);
    }

    @Override
    public User getUserByTweetID(Long tweet_id) {
        Query query = em.createQuery("select u " + 
                                     "from User u " +
                                     "join u.tweets t " +
                                     "where t.id = ?1");
        query.setParameter( 1, tweet_id ); 
        return (User) query.getSingleResult();
    }
}
