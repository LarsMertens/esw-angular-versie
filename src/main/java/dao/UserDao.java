package dao;

import java.util.List;
import model.Tweet;
import model.User;

public interface UserDao {
    
    /**
     * Creates an user model
     * @param u = user object instance
     */
    void create(User u);

    /**
     * Get all users
     * @return a List of user model objects
     */
    List<User> findAll();

    /**
     * Get a single user by id
     * @param id - id of the user
     * @return a single user model object
     */
    User find(Long id);
    
    /**
     * Search for all users with string like input
     * @param input - input string
     * @return list of users with given string
     */
    List<User> search(String input);
    
    /**
     * Generates an user timeline
     * @param id - of user
     * @return timeline of user
     */
    List<Tweet> getTimeline(Long id);
    
    /**
     * Gets the latest tweets from an user
     * @param id - id from the user
     * @return list of latest tweets
     */
    List<Tweet> getLatestTweets(Long id);
    
    /**
     * Gets a list of all followers by user id
     * @param id - id from the user
     * @return all followers from the given user
     */
    List<User> getFollowers(Long id);
    
    /**
     * Adds an follower to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be followed
     */
    void addFollower(Long my_id, Long user_id); 
    
    /**
     * Removes follower by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a follower
     */
    public void removeFollower(Long my_id, Long user_id);
    
    /**
     * Gets a list of all following by user id
     * @param id - id from the user
     * @return all following from the given user
     */
    List<User> getFollowing(Long id);
    
    /**
     * Adds an following to an user
     * @param my_id - user id of me
     * @param user_id - user id of the user that needs to be following
     */
    void addFollowing(Long my_id, Long user_id); 
    
    /**
     * Removes following by user id
     * @param my_id - my user id
     * @param user_id - user id that need to be a following
     */
    void removeFollowing(Long my_id, Long user_id);
    
    /**
     * Get all mentions from an user
     * @param id - an user id
     * @return all mentions from an user 
     */
    List<Tweet> getMentions(Long id);
    
    /**
     * Adds an tweet as mention
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    void addMention(Long user_id, Long tweet_id); 
    
    List<Tweet> getFollowingTweets(Long id);
    
    boolean isFollowing(Long my_id, Long following_id);
    
    User getUserByName(String username);
    
    User getUserByTweetID(Long tweet_id);
}
