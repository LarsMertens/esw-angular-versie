package dao;

import Event.TweetEvent;
import java.util.List;
import model.Tweet;

public interface TweetDao {
    
    /**
     * Creates a tweet model
     * @param t = tweet object instance
     */
    boolean create(Tweet t, Long id);

    /**
     * Get all tweets
     * @return a List of tweet model objects
     */
    List<Tweet> findAll();

    /**
     * Get a single tweet by id
     * @param id - id of the tweet
     * @return a single tweet model object
     */
    Tweet find(Long id);
    
    /**
     * Search for all tweets with string like input
     * @param input - input string
     * @return list of tweets with given string
     */
    List<Tweet> search(String input);
    
    /**
     * Gets a list of all tweets that are trends
     * @return list of trends
     */
    List<Tweet> getTrends();   
    
    /**
     * Deletes an tweet
     * @param id - tweet id
     */
    void deleteTweet(Long id);
    
    /**
     * Adds an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    void addFavoriteTweet(Long user_id, Long tweet_id); 
    
    /**
     * Removes an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */
    void removeFavoriteTweet(Long user_id, Long tweet_id); 
    
    boolean isFavoriteTweet(Long user_id, Long tweet_id);
    
    List<Tweet> getFavoriteTweets(Long user_id);
    

    public boolean create(TweetEvent tweet);
            }
