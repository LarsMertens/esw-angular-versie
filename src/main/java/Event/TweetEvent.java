package Event;

import model.Tweet;

public class TweetEvent {

    private Tweet tweet;
    private Long id;

    public TweetEvent(Tweet tweet, Long id) {
        this.tweet = tweet;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }
}
