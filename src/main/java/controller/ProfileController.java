package controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import model.Profile;
import org.primefaces.model.UploadedFile;
import service.ProfileService;
import service.UserService;

@Path("profile")
@Stateless
public class ProfileController {
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private UserService userService;
   
    public ProfileController(){}
    
    @GET
    @Path("/get/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Profile get(@PathParam("id") Long id){
        return profileService.getProfile(id);
    }
    
    @GET
    @Path("/getAll")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Profile> getAll(){
        return profileService.getProfiles();
    }
 
    @POST
    @Path("/upload/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.MULTIPART_FORM_DATA})
    @Produces({MediaType.APPLICATION_JSON})
    public void upload(Profile profile, @PathParam("user_id") Long user_id){
        Profile updateProfile = userService.getUser(user_id).getProfile(); 
        updateProfile.setBiography(profile.getBiography());
        updateProfile.setLocation(profile.getLocation());
        updateProfile.setWebsite(profile.getWebsite());
        profileService.updateProfile(updateProfile);
        
        //Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        //String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        //InputStream fileContent = filePart.getInputStream();
        
//        UploadedFile uploadedPhoto = file;
//        UploadedFile uploadedPhotoBackground = imageFile;
//        String filePath="c:/Users/lars_/Documents/NetBeansProjects/Kwetter/src/main/webapp/uploads/";
//        String filename;
//        byte[] bytes=null;
// 
//        if (null!=uploadedPhoto) {
//            bytes = uploadedPhoto.getContents();
//            filename = uploadedPhoto.getFileName();
//            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
//            stream.write(bytes);
//            stream.close();
//            profile.setAvatar(filename);
//            
//            bytes = uploadedPhotoBackground.getContents();
//            filename = uploadedPhotoBackground.getFileName();
//            stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
//            stream.write(bytes);
//            stream.close();
//            profile.setImage(filename);
//        }
//        profile.setBiography(profile.getBiography());
//        profile.setLocation(profile.getLocation());
//        profile.setWebsite(profile.getWebsite());
//        profileService.updateProfile(profile);
    }
}
