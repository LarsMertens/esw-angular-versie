package controller;

import javax.ws.rs.core.Application;

/**
 * Set application path to api
 */
@javax.ws.rs.ApplicationPath("api")
public class ApplicationPath extends Application {   
}