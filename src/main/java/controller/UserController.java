package controller;

import bean.AuthBean;
import java.security.Principal;
import java.util.List;
import javax.ejb.Stateless;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import model.Tweet;
import model.User;
import service.ProfileService;
import service.TweetService;
import service.UserService;

@Path("user")
@Stateless
public class UserController {
    
    @Inject
    private UserService userService;
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private TweetService tweetService;
   
    @Inject Principal userPrincipal;
    
    public UserController(){}
            
    @GET
    @Path("/getLoggedInUser")
    @Produces({MediaType.APPLICATION_JSON})
    public User getLoggedInUser(){
         String username = userPrincipal.getName();
         return userService.getUserByName(username);
    }
    
    @GET
    @Path("/get/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public User get(@PathParam("id") Long id){
        return userService.getUser(id);
    }
    
    @GET
    @Path("/getAll")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getAll(){
        return userService.getUsers();
    }
    
    @GET
    @Path("/getAllFollowing/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getAllFollowing(@PathParam("id") Long id){
        return userService.getFollowing(id);
    }

    @GET
    @Path("/getAllFollowers/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getAllFollowers(@PathParam("id") Long id){
        return userService.getFollowers(id);
    }
    
    @GET
    @Path("/getLatestTweets/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getLatestTweets(@PathParam("id") Long id){
        return userService.getLatestTweets(id);
    }
    
    @GET
    @Path("/countFollowing/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> countFollowing(@PathParam("id") Long id){
        return userService.getFollowing(id);
    }
    
    @GET
    @Path("/countFollowers/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> countFollowers(@PathParam("id") Long id){
        return userService.getFollowers(id);
    }
    
    @POST
    @Path("/addFollowing/{my_id}/{following_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFollowing(@PathParam("my_id") Long my_id, @PathParam("following_id") Long following_id){
        userService.addFollowing(my_id, following_id);
        //userService.addFollower(following_id, my_id);
    }
    
    @POST
    @Path("/removeFollowing/{my_id}/{following_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void removeFollowing(@PathParam("my_id") Long my_id, @PathParam("following_id") Long following_id){
        userService.removeFollowing(my_id, following_id);
    }
    
    @POST
    @Path("/addFollower/{my_id}/{following_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFollower(@PathParam("my_id") Long my_id, @PathParam("following_id") Long following_id){
        userService.addFollower(following_id, my_id);
    }
    
    @POST
    @Path("/removeFollower/{my_id}/{following_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void removeFollower(@PathParam("my_id") Long my_id, @PathParam("following_id") Long following_id){
        userService.removeFollower(following_id, my_id);
    }
    
    @GET
    @Path("/isFollowing/{my_id}/{following_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public boolean isFollowing(@PathParam("my_id") Long my_id, @PathParam("following_id") Long following_id){
        return userService.isFollowing(my_id, following_id);
    }
    
    @GET
    @Path("/getUserByTweetID/{tweet_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public User getUserByTweetID(@PathParam("tweet_id") Long tweet_id){
        return userService.getUserByTweetID(tweet_id);
    }
    
    @GET
    @Path("/search/{search_value}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> search(@PathParam("search_value") String search_value){
        return userService.searchUsers(search_value);
    }
}
