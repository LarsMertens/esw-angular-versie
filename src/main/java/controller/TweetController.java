package controller;

import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Tweet;
import model.User;
import service.TweetService;
import service.UserService;

@Path("tweet")
@Stateless
public class TweetController {
    
    @Inject
    private TweetService tweetService;
    
    @Inject
    private UserService userService;
    
    public TweetController(){}
    
    @GET
    @Path("/get/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Tweet get(@PathParam("id") Long id){
        return tweetService.getTweet(id);
    }
    
    @GET
    @Path("/getAll")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getAll(){
        return tweetService.getTweets();
    }
    
    @GET
    @Path("/countMyTweets/{user_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public int countMyTweets(@PathParam("user_id") Long user_id){
        return userService.getUser(user_id).getTweets().size();
    }
    
    @GET
    @Path("/getMentions/{user_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getMentions(@PathParam("user_id") Long user_id){
        return userService.getMentions(user_id);    
    }
    
    @GET
    @Path("/getTimeline/{user_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> getTimeline(@PathParam("user_id") Long user_id){
        List<Tweet> holdTweets = new ArrayList<Tweet>();
        List<Tweet> timelineTweets = userService.getUser(user_id).getTweets();
        List<User> followingUsers = userService.getUser(user_id).getFollowing();
        holdTweets.addAll(timelineTweets);
        for(int i = 0; i < followingUsers.size(); i++){
            holdTweets.addAll(followingUsers.get(i).getTweets());
        }
        Collections.sort(holdTweets, Collections.reverseOrder());
        if(holdTweets.size() >= 20){
            holdTweets = holdTweets.subList(0, 20);
        } 
        return holdTweets; 
    }
    
    @POST
    @Path("/addTweet/{user_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addTweet(@PathParam("user_id") Long user_id, Tweet tweet){
        if(tweet.getContent().contains("#")){
            String subjects[] = tweet.getContent().split("\\#");
            String sub = subjects[1];
            if(sub.contains(" ")){
                sub = sub.substring(0, sub.indexOf(" ")); 
            }
            tweet.setSubject(sub);
        }
        tweet.setCreated_at(new Date());
        tweet.setUpdated_at(new Date());
        tweetService.addTweet(tweet, user_id);
        if(tweet.getContent().contains("@")){
            String usernames[] = tweet.getContent().split("\\@");
            String username = usernames[1];
            if(username.contains(" ")){
                username = username.substring(0, username.indexOf(" ")); 
            }
            if(userService.getUserByName(username) != null){
                userService.addMention(userService.getUserByName(username).getId(), tweet.getId());
            }
        }
    }
    
    @POST
    @Path("/addFavoriteTweet/{user_id}/{tweet_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void addFavoriteTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long tweet_id){
        tweetService.addFavoriteTweet(user_id, tweet_id);
    }
    
    @POST
    @Path("/removeFavoriteTweet/{user_id}/{tweet_id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public void removeFavoriteTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long tweet_id){
        tweetService.removeFavoriteTweet(user_id, tweet_id);
    }

    @GET
    @Path("/search/{search_value}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Tweet> search(@PathParam("search_value") String search_value){
        return tweetService.searchTweets(search_value);
    }
   
}
