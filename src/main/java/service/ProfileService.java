package service;

import dao.JPA;
import dao.ProfileDao;
import dao.ProfileDaoImp;
import java.util.List;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Profile;

@Stateless
public class ProfileService {

    @Inject @JPA
    private ProfileDao profileDao;

    public ProfileService() {
        profileDao = new ProfileDaoImp();
    }

    /**
     * add a profile
     * @param p - profile
     */
    public void addProfile(Profile p) {
        profileDao.create(p);
    }
    
    /**
     * Update a profile
     * @param p - profile
     * @return updated profile
     */
    public Profile updateProfile(Profile p) {
        return profileDao.update(p);
    }

    /**
     * Get list of profiles
     * @return list of profile objects
     */
    public List<Profile> getProfiles() {
        return profileDao.findAll();
    }
    
    /**
     * Get single profile
     * @param id - profile id
     * @return selected profile
     */
    public Profile getProfile(Long id){
        return profileDao.find(id);
    }
    
}
