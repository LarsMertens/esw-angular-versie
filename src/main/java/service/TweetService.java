package service;

import Event.TweetEvent;
import dao.JPA;
import dao.TweetDao;
import dao.TweetDaoImp;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import model.Tweet;

@Stateless

public class TweetService {

    @Inject @JPA
    private TweetDao tweetDao;
    
        @Inject
    private Event<TweetEvent> tweetEvent;

    public TweetService() {
        tweetDao = new TweetDaoImp();
    }

    /**
     * Add a tweet
     * @param t - tweet
     * @return true or false
     */

    public boolean addTweet(Tweet t, Long id) {
        
        if (t != null) {
            tweetEvent.fire(new TweetEvent(t, id));
            
//          studentWS.sendToAllSessions(student);
//          studentDao.addStudent(student);
    return true;
        }
        //tweetDao.create(t, id);
        return false;
    }

    /**
     * Get list of tweets
     * @return list of tweets
     */
    public List<Tweet> getTweets() {
        return tweetDao.findAll();
    }
    
    /**
     * Return list of tweets by searched input
     * @param input - search by content in tweets
     * @return list of tweets
     */
    public List<Tweet> searchTweets(String input){
        return tweetDao.search(input);
    }
    
    /**
     * Get single tweet
     * @param id - tweet id
     * @return selected tweet
     */
    public Tweet getTweet(Long id){
        return tweetDao.find(id);
    }
    
    /**
     * Delete a tweet
     * @param id - id of tweet that got to be removed
     */

    public void deleteTweet(Long id){
        tweetDao.deleteTweet(id);
    }
    
    /**
     * Get trents of all tweets
     * @return list of tweets that are trents
     */
    public List<Tweet> getTrends(){
        return tweetDao.getTrends();
    }
    
    /**
     * Set a tweetDao
     * @param tweetDao - dao that got to be set as tweetdao 
     */
    public void setTweetDao(TweetDao tweetDao) {
        this.tweetDao = tweetDao;
    }
    
    /**
     * Adds an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */

    public void addFavoriteTweet(Long user_id, Long tweet_id){
        tweetDao.addFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * Removes an tweet as favorite
     * @param user_id - user id
     * @param tweet_id - tweet id
     */

    public void removeFavoriteTweet(Long user_id, Long tweet_id){
        tweetDao.removeFavoriteTweet(user_id, tweet_id);
    }
    

    public boolean isFavoriteTweet(Long user_id, Long tweet_id){
        return tweetDao.isFavoriteTweet(user_id, tweet_id);
    }
    

    public List<Tweet> getFavoriteTweets(Long user_id){
        return tweetDao.getFavoriteTweets(user_id);
    }
}
