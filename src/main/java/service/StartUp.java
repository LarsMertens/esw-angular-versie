package service;

import dao.GroupDaoJPA;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.RunAs;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import model.Group;
import model.Profile;
import model.Tweet;
import model.User;
import util.PasswordHash;

@Singleton
@Startup
public class StartUp {

    @Inject
    private ProfileService profileService;
    
    @Inject
    private TweetService tweetService;
    
    @Inject
    private UserService userService;
    
    @Inject
    private GroupDaoJPA groupDao;
      
    public StartUp() {}
     
    @PostConstruct
    private void intData(){
        
        Group adminGroup = new Group(Group.ADMIN_GROUP);
        Group userGroup = new Group(Group.USER_GROUP);
        groupDao.create(adminGroup);
        groupDao.create(userGroup);
        
        // 1
        User user = new User("LarsMertens","lars_ke@hotmail.com", PasswordHash.stringToHash("test"),
                             "user","Lars", "Mertens", 
                             new Date(1991,11,27));  
        user.getGroup().add(userGroup);
        userService.addUser(user); 

        Tweet tweet = new Tweet(1L, "HelloWorld", "Hello World!");

        Profile profile = new Profile("Hello Im lars mertens 25 years old from The Netherlands", 
                                      "The Netherlands", 
                                      "www.larsmertens.nl", 
                                      "larsmertens.jpg", 
                                      "larsmertens-background.jpg");

        profileService.addProfile(profile);
        tweetService.addTweet(tweet, user.getId());
        user.setProfile(profile);
        profile.setUser(user);
        
        // 2
        User user2 = new User("JalishaJanssen","jalisha.janssen@hotmail.com", PasswordHash.stringToHash("test"),
                             "user","Jalisha", "Janssen", 
                             new Date(1998,7,21));  
        user2.getGroup().add(userGroup);
        userService.addUser(user2); 

        Tweet tweet2 = new Tweet(2L, "Studying", "Late nate studying #notsogreat");

        Profile profile2 = new Profile("Hello Im Jalisha Janssen 19 years old from The Netherlands", 
                                      "The Netherlands", 
                                      "none", 
                                      "jalishajanssen.jpg", 
                                      "jalishajanssen-background.jpg");

        profileService.addProfile(profile2);
        tweetService.addTweet(tweet2, user2.getId());
        user2.setProfile(profile2);
        profile2.setUser(user2);
        
        // 3
        User user3 = new User("Jamjorn","nick.rademakers@gmail.com", PasswordHash.stringToHash("test"),
                             "user","Nick", "Rademakers", 
                             new Date(1992,9,18)); // dunno xD  
        user3.getGroup().add(userGroup);
        userService.addUser(user3); 

        Tweet tweet3 = new Tweet(3L, "Java", "Working on ESW #java");

        Profile profile3 = new Profile("Hello Im Nick Rademakers 25 years old from The Netherlands", 
                                      "The Netherlands", 
                                      "none", 
                                      "nickrademakers.jpg", 
                                      "nickrademakers-background.jpg");

        profileService.addProfile(profile3);
        tweetService.addTweet(tweet3, user3.getId());
        user3.setProfile(profile3);
        profile3.setUser(user3);
        
        // 4 
        User user4 = new User("TommieMertens","tommiemertens@gmail.com", PasswordHash.stringToHash("test"),
                             "user","Tommie", "Mertens", 
                             new Date(1986,8,28)); // dunno xD  
        user4.getGroup().add(userGroup);
        userService.addUser(user4); 

        Tweet tweet4 = new Tweet(4L, "Geography", "Teaching Geography");

        Profile profile4 = new Profile("Hello Im Tommie Mertens 32 years old from The Netherlands", 
                                      "The Netherlands", 
                                      "none", 
                                      "tommiemertens.jpg", 
                                      "tommiemertens-background.jpg");

        profileService.addProfile(profile4);
        tweetService.addTweet(tweet4, user4.getId());
        user4.setProfile(profile4);
        profile4.setUser(user4);
        
        // 5
        User user5 = new User("WimMertens","wimmertens@gmail.com", PasswordHash.stringToHash("test"),
                             "user","Wim", "Mertens", 
                             new Date(1956,8,7));  
        user5.getGroup().add(userGroup);
        userService.addUser(user5); 

        Tweet tweet5 = new Tweet(5L, "RedAlert", "Playing #RedAlert");

        Profile profile5 = new Profile("Hello Im Wim Mertens 61 years old from The Netherlands", 
                                      "The Netherlands", 
                                      "none", 
                                      "wimmertens.jpg", 
                                      "wimmertens-background.jpg");

        profileService.addProfile(profile5);
        tweetService.addTweet(tweet5, user5.getId());
        profile5.setUser(user5);
        user5.setProfile(profile5);
        
        // 6
        User user6 = new User("WoW","info@wow.com", PasswordHash.stringToHash("test"),
                              "user","World", "Of Warcraft", 
                              new Date(2004,1,1));  
        user6.getGroup().add(userGroup);
        userService.addUser(user6); 

        Tweet tweet6 = new Tweet(6L, "Azeroth", "Azeroth Vibes");

        Profile profile6 = new Profile("This is the World Of Warcraft official fanpage", 
                                      "The Netherlands", 
                                      "none", 
                                      "wow.jpg", 
                                      "wow-background.jpg");

        profileService.addProfile(profile6);
        tweetService.addTweet(tweet6, user6.getId());
        profile6.setUser(user6);
        user6.setProfile(profile6);
        
        // 7
        User user7 = new User("4Bytes","info@4bytes.com", PasswordHash.stringToHash("test"),
                              "user","4Bytes", "Webdevelopment", 
                              new Date(2017,2,1));  
        user7.getGroup().add(userGroup);
        userService.addUser(user7); 

        Tweet tweet7 = new Tweet(7L, "4Bytes", "4Bytes coding...");

        Profile profile7 = new Profile("This is the 4Bytes official page", 
                                      "The Netherlands", 
                                      "none", 
                                      "4bytes.jpg", 
                                      "4bytes-background.jpg");

        profileService.addProfile(profile7);
        tweetService.addTweet(tweet7, user7.getId());
        profile7.setUser(user7);
        user7.setProfile(profile7);
        
        // 8
        User user8 = new User("NielsMertens","nielsmertens@gmail.com", PasswordHash.stringToHash("test"),
                              "user","Niels", "Mertens", 
                              new Date(1984,12,31));  //dno
        user8.getGroup().add(userGroup);
        userService.addUser(user8); 

        Tweet tweet8 = new Tweet(8L, "HouseBuilding", "Building our house in the middle of our street");

        Profile profile8 = new Profile("This is the page of Niels Mertens", 
                                      "The Netherlands", 
                                      "none", 
                                      "nielsmertens.jpg", 
                                      "nielsmertens-background.jpg");

        profileService.addProfile(profile8);
        tweetService.addTweet(tweet8, user8.getId());
        profile8.setUser(user8);
        user8.setProfile(profile8);
        
        // 9
        User user9 = new User("JariMertens","jarimertens@gmail.com", PasswordHash.stringToHash("test"),
                              "user","Jari", "Mertens", 
                              new Date(1995,11,5));  //dno
        user9.getGroup().add(userGroup);
        userService.addUser(user9); 

        Tweet tweet9 = new Tweet(9L, "Birthday", "Celebrating my birthday");

        Profile profile9 = new Profile("This is the page of Jari Mertens", 
                                      "The Netherlands", 
                                      "none", 
                                      "jarimertens.jpg", 
                                      "jarimertens-background.jpg");

        profileService.addProfile(profile9);
        tweetService.addTweet(tweet9, user9.getId());
        profile9.setUser(user9);
        user9.setProfile(profile9);
        
        // 10
        User user10 = new User("Christmas","christmas@gmail.com", PasswordHash.stringToHash("test"),
                               "user","Christmas", "2017", 
                               new Date(2017,12,24));  
        user10.getGroup().add(userGroup);
        userService.addUser(user10); 

        Tweet tweet10 = new Tweet(10L, "Christmas", "Last Christmas...");
 
        Profile profile10 = new Profile("This is the page regarding christmas 2017", 
                                      "Global", 
                                      "none", 
                                      "christmas.jpg", 
                                      "christmas-background.jpg");

        profileService.addProfile(profile10);
        tweetService.addTweet(tweet10, user10.getId());
        profile10.setUser(user10);
        user10.setProfile(profile10);
    }
}
