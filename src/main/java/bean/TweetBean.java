package bean;

import java.io.IOException;
import model.Tweet;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import model.User;
import service.TweetService;
import service.UserService;

@Named(value = "tweetBean")
@RequestScoped
public class TweetBean implements Serializable{
    
    @Inject
    private TweetService tweetService;
    
    @Inject
    private UserService userService;

    private Long id;
    private String subject;
    
    @Size(min=3, max=160)
    private String content;
    private Date created_at;
    private Date updated_at;
    private boolean trent;
    private User user;
    private List<Tweet> tweets;
    private String filter;

    public TweetBean(){}
    
//    @PostConstruct
//    public void init() {
//        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
//        AuthBean authBean = (AuthBean) elContext.getELResolver().getValue(elContext, null, "authBean");
//        if(authBean.isLoggedIn()){
//            this.setUser(authBean.getUser());
//        }
//        tweets = getTimeline();
//    }
    
    public void addTweet() throws IOException{
        Tweet tweet = new Tweet(this.getUser().getId(), null, content);
        if(content.contains("#")){
            String subjects[] = content.split("\\#");
            String sub = subjects[1];
            if(sub.contains(" ")){
                sub = sub.substring(0, sub.indexOf(" ")); 
            }
            tweet.setSubject(sub);
        }
        tweet.setCreated_at(new Date());
        tweet.setUpdated_at(new Date());
        tweetService.addTweet(tweet, this.getUser().getId());
        if(content.contains("@")){
            String usernames[] = content.split("\\@");
            String username = usernames[1];
            if(username.contains(" ")){
                username = username.substring(0, username.indexOf(" ")); 
            }
            if(this.getUserService().getUserByName(username) != null){
                userService.addMention(this.getUserService().getUserByName(username).getId(), tweet.getId());
            }
        }
        tweets = getTimeline();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }
    
    /**
     * @TODO 
     * @return 
     */
    public List<Tweet> getTimeline(){
        if (filter != null && filter.length() > 0) {
            List<Tweet> filtered = tweetService.searchTweets(filter);
            this.setTweets(filtered);
            return filtered;
        } else {
            List<Tweet> timelineTweets = this.getUser().getTweets();
            List<User> followingUsers = this.getUser().getFollowing();
            for(int i = 0; i < followingUsers.size(); i++){
                timelineTweets.addAll(followingUsers.get(i).getTweets());
            }
            Collections.sort(timelineTweets, Collections.reverseOrder());
            if(timelineTweets.size() >= 20){
                this.setTweets(timelineTweets.subList(0, 20));
                timelineTweets = timelineTweets.subList(0, 20);
            } else {
                this.setTweets(timelineTweets);
            }
            return timelineTweets;
        }
    }
    
    /**
     * @TODO
     * @return 
     */
    public List<Tweet> getMentions(){
        return userService.getMentions(this.getUser().getId());      
    }
    
    /**
     * @TODO
     * @return 
     */
    public int countMyTweets(){
        return userService.getUser(1L).getTweets().size();
    }
    
    /**
     * @TODO
     * @return 
     */
    public Tweet getLatestTweet(){
        if(userService.getUser(1L).getTweets().size() > 0){
            return userService.getUser(1L).getTweets().get(userService.getUser(1L).getTweets().size()-1);
        } else{
            return null;
        }
    }
    
    /**
     * @TODO 
     * @return 
     */
    public List<Tweet> getTrends(){
        return tweetService.getTrends();
    }

    public TweetService getTweetService() {
        return tweetService;
    }

    public void setTweetService(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isTrent() {
        return trent;
    }

    public void setTrent(boolean trent) {
        this.trent = trent;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }
    
    public List<Tweet> getFavoriteTweets(Long user_id){
        return tweetService.getFavoriteTweets(user_id);
    }
    
    public List<Tweet> getFavoriteTweetsUser(){
        Long user_id = this.getUser().getId();
        return tweetService.getFavoriteTweets(user_id);
    }
    
    /**
     * @TODO
     */
    public void addFavoriteTweet(Long tweet_id){
        Long user_id = this.getUser().getId();
        tweetService.addFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * @TODO
     * @param tweet_id 
     */
    public void removeFavoriteTweet(Long tweet_id){
        Long user_id = this.getUser().getId();
        tweetService.removeFavoriteTweet(user_id, tweet_id);
    }
    
    /**
     * @TODO
     * @param tweet_id
     * @return 
     */
    public boolean isFavoriteTweet(Long tweet_id){
        Long user_id = this.getUser().getId();
        return tweetService.isFavoriteTweet(user_id, tweet_id);
    }
    
}
