package bean;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import model.Profile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import model.User;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import service.ProfileService;
import service.UserService;

@Named(value = "profileBean")
@RequestScoped
@ManagedBean
public class ProfileBean implements Serializable{
    
    @Inject
    private ProfileService profileService;
    
    @Inject
    private UserService userService;

    private Long id;
    private Long user_id;
    
    @Size(min=3, max=160)
    private String biography;
    @Size(max=160)
    private String location;
    @Size(max=160)
    private String website;
    @NotNull
    private String avatar;
    @NotNull
    private String image;
    private Date created_at;
    private Date updated_at;
    @NotNull
    private UploadedFile file;
    @NotNull
    private UploadedFile imageFile;
    private User user;
    
//    @PostConstruct
//    public void init() {
//        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
//        AuthBean authBean = (AuthBean) elContext.getELResolver().getValue(elContext, null, "authBean");
//        if(authBean.isLoggedIn()){
//            this.setUser(authBean.getUser());
//        }
//    }

    public void upload() throws IOException {
        UploadedFile uploadedPhoto = getFile();
        UploadedFile uploadedPhotoBackground = getImageFile();
        String filePath="c:/Users/lars_/Documents/NetBeansProjects/Kwetter/src/main/webapp/uploads/";
        String filename;
        Profile profile = this.getUser().getProfile();
        byte[] bytes=null;
 
        if (null!=uploadedPhoto) {
            bytes = uploadedPhoto.getContents();
            filename = uploadedPhoto.getFileName();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
            stream.write(bytes);
            stream.close();
            profile.setAvatar(filename);
            
            bytes = uploadedPhotoBackground.getContents();
            filename = uploadedPhotoBackground.getFileName();
            stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
            stream.write(bytes);
            stream.close();
            profile.setImage(filename);
        }
        profile.setBiography(biography);
        profile.setLocation(location);
        profile.setWebsite(website);
        profileService.updateProfile(profile);
    }
  
    public ProfileService getProfileService() {
        return profileService;
    }

    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UploadedFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(UploadedFile imageFile) {
        this.imageFile = imageFile;
    }
    
    
}
