package bean;

import model.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import model.Profile;
import model.Tweet;
import service.UserService;

@Named(value = "userBean")
@RequestScoped
public class UserBean implements Serializable{
    
    @Inject
    private UserService userService;

    private Long id;
    private String username;
    private String email;
    private String password;
    private String type;
    private String firstname;
    private String lastname;
    private Date date_of_birth;
    private Date created_at;
    private Date updated_at;
    private Profile profile;
    private List<Tweet> tweets;
    private List<Tweet> favorites;
    private List<Tweet> mentions;
    private List<User> following;
    private List<User> followers;
    private List<User> users;
    private String filter;
    private Long userID;
    private User user;
    
    @PostConstruct
    public void init() {
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        AuthBean authBean = (AuthBean) elContext.getELResolver().getValue(elContext, null, "authBean");
        if(authBean.isLoggedIn()){
            this.setUser(authBean.getUser());
        }
        users = getTheUsers();
    }
    
    public List<User> getTheUsers(){
        if (filter != null && filter.length() > 0) {
            List<User> filtered = userService.searchUsers(filter);
            this.setUsers(filtered);
            return filtered;
        } else {
            List<User> allUsers = userService.getUsers();
            User myself = userService.getUser(this.getUser().getId());
            allUsers.remove(myself);
            this.setUsers(allUsers);
            return allUsers;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUserById(){
        User user = userService.getUser(userID);
        return user;
    }
    
    public User getUserByName(String username){
        return userService.getUserByName(username);
    }
    
    public List<User> getAllFollowing(){
        return userService.getFollowing(this.getUser().getId());
    }
    
    public List<User> getAllFollowers(){
        return userService.getFollowers(this.getUser().getId());
    }
    
    public List<Tweet> getLatestTweets(){
       return userService.getLatestTweets(this.getUser().getId());
    }
    
    public List<Tweet> getLatestTweetsUser(){
       return userService.getLatestTweets(userID);
    }
    
    public void addFollowing(Long following_id){
        Long my_id = this.getUser().getId();
        userService.addFollowing(my_id, following_id);
        userService.addFollower(following_id, my_id);
    }
    
    public void removeFollowing(Long following_id){
        Long my_id = this.getUser().getId();
        userService.removeFollowing(my_id, following_id);
        userService.removeFollower(following_id, my_id);
    }
    
    public boolean isFollowing(Long following_id){
        Long my_id = this.getUser().getId();
        return userService.isFollowing(my_id, following_id);
    }
    
//    public User getUserByTweetID(Long tweet_id){
//        return userService.getUserByTweetID(tweet_id);
//    }
    
    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public List<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(List<Tweet> tweets) {
        this.tweets = tweets;
    }

    public List<Tweet> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<Tweet> favorites) {
        this.favorites = favorites;
    }

    public List<Tweet> getMentions() {
        return mentions;
    }

    public void setMentions(List<Tweet> mentions) {
        this.mentions = mentions;
    }

    public List<User> getFollowing() {
        return following;
    }

    public void setFollowing(List<User> following) {
        this.following = following;
    }

    public List<User> getFollowers() {
        return followers;
    }

    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }
    
    
}
