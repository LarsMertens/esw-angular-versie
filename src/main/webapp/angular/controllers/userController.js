angular.module('userCtrl', [])

    .controller('userController', function($scope, $http, $window, $timeout, $location, User, Tweet, Profile) {

        $scope.loading = false;
        $scope.isSaving = false;
        $scope.object = {};

        $scope.initUserTimeline = function() {
            User.getLoggedInUser()
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;                   
                    $scope.getTimeline($scope.object.id);
                   
                    angular.forEach($scope.object.favorites, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                if(!$scope.object.favorites[key].user){
                                    var user = success.data;
                                    $scope.object.favorites[key].user = user;
                                } else {
                                    $scope.object.favorites[key].user = "";
                                }
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });  
                    angular.forEach($scope.object.mentions, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                if(!$scope.object.mentions[key].user){
                                    var user = success.data;
                                    $scope.object.mentions[key].user = user;
                                }
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });     
                    $scope.countFollowing($scope.object.id);    
                    $scope.countFollowers($scope.object.id);    
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.initOtherUserTimeline = function() {
            var paramValue = getUrlParameter('userID');; 
            
            User.get(paramValue)
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;                   
                    $scope.getTimeline($scope.object.id);
                   
                    angular.forEach($scope.object.favorites, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                if(!$scope.object.favorites[key].user){
                                    var user = success.data;
                                    $scope.object.favorites[key].user = user;
                                } else {
                                    $scope.object.favorites[key].user = "";
                                }
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });  
                    angular.forEach($scope.object.mentions, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                if(!$scope.object.mentions[key].user){
                                    var user = success.data;
                                    $scope.object.mentions[key].user = user;
                                }
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });     
                    $scope.countFollowing($scope.object.id);    
                    $scope.countFollowers($scope.object.id);    
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        
        $scope.initUsersList = function(){
            User.getLoggedInUser()
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.getAll();
                    $scope.getAllFollowers($scope.object.id);
                    $scope.getAllFollowing($scope.object.id);
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.initMyTimeline = function(){
            User.getLoggedInUser()
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.getLatestTweets($scope.object.id);
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getTimeline = function($user_id){
            Tweet.getTimeline($user_id)
                .then(function (success) {
                    $scope.timelineTweets = success.data;
                    $scope.loading = false;                    
                    angular.forEach($scope.timelineTweets, function(value, key) {
                        User.getUserByTweetID(value.id).then(function (success) {
                            if(!$scope.timelineTweets[key].user){
                                var user = success.data;
                                $scope.timelineTweets[key].user = user;
                            }
                        }).catch(function (e) {
                            console.log("got an error in the process", e);
                        });  
                    });    
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getLoggedInUser = function(){
            User.getLoggedInUser()
                .then(function (success) {
                    $scope.object = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };

        /**
         * Get the data of a single object
         *
         * @param {type} $id
         * @returns {undefined}
         */
        $scope.get = function($id){
            User.get($id)
                .then(function (success) {
                    $scope.object = success.data; 
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        /**
         * Get the data of all objects in database
         */
        $scope.getAll = function(){
            User.getAll()
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getAllFollowing = function($id){
            User.getAllFollowing($id)
                .then(function (success) {
                    $scope.followingData = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getAllFollowers = function($id){
            User.getAllFollowers($id)
                .then(function (success) {
                    $scope.followersData = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getLatestTweets = function($id){
            User.getLatestTweets($id)
                .then(function (success) {
                    $scope.latestTweets = success.data;
                    
                        
                        
                        console.log($scope.object);
                        angular.forEach($scope.latestTweets, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                if(!$scope.latestTweets[key].user){
                                    var user = success.data;
                                    $scope.latestTweets[key].user = user;
                                }
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });  
                    console.log($scope.object);
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.addFollowing = function($my_id, $following_id){
            User.addFollowing($my_id, $following_id)
                .then(function (success){
                    console.log("following aangemaakt");  
                    $scope.initUsersList();
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
            User.addFollower($my_id, $following_id)
                .then(function (success){
                    console.log("following aangemaakt");  
                    $scope.initUsersList();
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });          
        };
        
        $scope.removeFollowing = function($my_id, $following_id){
            User.removeFollowing($my_id, $following_id)
                .then(function (success){
                    console.log("following verwijderd");
                    $scope.initUsersList();
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
            User.removeFollower($my_id, $following_id)
                .then(function (success){
                    console.log("following aangemaakt");  
                    $scope.initUsersList();
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });       
        };

        $scope.isFollowing = function($my_id, $following_id){
            
            
            var check = false;
            angular.forEach($scope.followingData, function(value, key) {
               // my_id = 1 , value.id = 3
               
                if($following_id === value.id){
                    check = true;
                }
            });  
            return check;
        };
        
        $scope.getUserByTweetID = function($id){
            User.getUserByTweetID($id)
                .then(function (success) {
                    return success.data;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.countFollowing = function($user_id){
            User.countFollowing($user_id)
                .then(function (success) {
                    if(success.data.length > 0){
                        $scope.followingSize = success.data.length;
                    } else {
                        $scope.followingSize = 0;
                    }
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.countFollowers = function($user_id){
            User.countFollowers($user_id)
                .then(function (success) {
                    if(success.data.length > 0){
                        $scope.followersSize = success.data.length;
                    } else {
                        $scope.followersSize = 0;
                    }
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
          
        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         */
        $scope.searchUsers = function($search_value) {
            if($search_value !== ''){
                $scope.loading = true;
                User.searchUsers($search_value)
                    .then(function (success) {
                        $scope.data = success.data;
                        $scope.loading = false;
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                $scope.initUsersList();
            }
        };
        
        /**
         * Search for data by string.
         * If data matches part of the string one or more results will be returned
         * Else if $search_value string is empty it will return the first page of the current data set
         *
         * @param $search_value : is the given input as string
         * @param $items_per_page : the number of items you want to show per page
         */
        $scope.searchTweets = function($search_value) {
            if($search_value !== ''){
                Tweet.search($search_value)
                    .then(function (success) {
                        $scope.timelineTweets = success.data;   
                        $scope.loading = false;
                        
                        angular.forEach($scope.timelineTweets, function(value, key) {
                            User.getUserByTweetID(value.id).then(function (success) {
                                var user = success.data;
                                $scope.timelineTweets[key].user = user;
                            }).catch(function (e) {
                                console.log("got an error in the process", e);
                            });  
                        });    
                    }).catch(function (e) {
                        console.log("got an error in the process", e);
                    });
            } else {
                $scope.initUserTimeline();
            }
        };
        
        
        
    });


