angular.module('tweetCtrl', [])

    .controller('tweetController', function($scope, $http, $window, $timeout, User,Tweet) {

        $scope.loading = false;
        $scope.isSaving = false;
        $scope.tweet = {};

        $scope.init = function($id) {
            
        };

        /**
         * Get the data of a single object
         *
         * @param {type} $id
         * @returns {undefined}
         */
        $scope.get = function($id){
            Tweet.get($id)
                .then(function (success) {
                    $scope.tweet = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        /**
         * Get the data of all objects in database
         */
        $scope.getAll = function(){
            Tweet.getAll()
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.countMyTweets = function($user_id){
            Tweet.countMyTweets($user_id)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.getMentions = function($user_id){
            Tweet.getMentions($user_id)
                .then(function (success) {
                    $scope.data = success.data;
                    $scope.loading = false;
                }).catch(function (e) {
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.isFavoriteTweet = function($user_id, $tweet_id){
            Tweet.isFavoriteTweet($user_id, $tweet_id)
                .then(function (success) {
                    return true;
                }).catch(function (e) {
                    return false;
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.addTweet = function($user_id){
            Tweet.addTweet($scope.tweet, $user_id)
                .then(function (success){
                    console.log("tweet aangemaakt");
                    console.log(success);    
                    $scope.initUserTimeline();    
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.addFavoriteTweet = function($user_id, $tweet_id){
            Tweet.addFavoriteTweet($user_id, $tweet_id)
                .then(function (success){
                    $scope.initUserTimeline();       
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
        };
        
        $scope.removeFavoriteTweet = function($user_id, $tweet_id){
            Tweet.removeFavoriteTweet($user_id, $tweet_id)
                .then(function (success){
                    $scope.initUserTimeline();    
                }).catch(function (errors){
                    console.log("got an error in the process", e);
                });
        };
        
    });





