angular.module('profileService', [])

    .factory('Profile', function($http) {

        var model = 'Kwetter/api/profile';

        return {
            get : function($id){
                return $http({
                    method: 'GET',
                    url: '/'+model+'/get/'+$id+''
                });
            },
            getAll : function() {
                return $http({
                    method: 'GET',
                    url: '/'+model+'/getAll'
                });
            },
            upload : function($object, $user_id) {
                return $http({
                    method: 'POST',
                    url: '/'+model+'/upload/'+$user_id+'',
                    data: $object
                });
            }
        };
    });