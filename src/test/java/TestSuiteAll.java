import tests.dao.ProfileDaoImpTest;
import tests.dao.TweetDaoImpTest;
import tests.dao.UserDaoImpTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.controller.ProfileControllerTest;
import tests.controller.TweetControllerTest;
import tests.controller.UserControllerTest;
import tests.model.ProfileTest;
import tests.model.TweetTest;
import tests.model.UserTest;
import tests.service.ProfileServiceTest;
import tests.service.TweetServiceTest;
import tests.service.UserServiceTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  ProfileControllerTest.class,
//  TweetControllerTest.class,
//  UserControllerTest.class, 
  ProfileServiceTest.class,
  TweetServiceTest.class,
  UserServiceTest.class,
  ProfileDaoImpTest.class,
  TweetDaoImpTest.class,
  UserDaoImpTest.class,
  ProfileTest.class,
  TweetTest.class,
  UserTest.class,
})

public class TestSuiteAll {

}