import tests.dao.ProfileDaoImpTest;
import tests.dao.TweetDaoImpTest;
import tests.dao.UserDaoImpTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  ProfileDaoImpTest.class,
  TweetDaoImpTest.class,
  UserDaoImpTest.class,
})

public class TestSuiteDao {

}