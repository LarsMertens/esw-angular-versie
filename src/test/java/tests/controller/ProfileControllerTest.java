package tests.controller;

import dao.ProfileDao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import model.Profile;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import service.ProfileService;
import service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class ProfileControllerTest {
    
    Client client;
    WebTarget root;
    static final String PATH = "/Kwetter/api/profile/";
    static final String BASEURL = "http://localhost:8080" + PATH;
    List<Profile> profileList = new ArrayList<Profile>();
    Profile profile;
    Profile testProfile;
     
    @Mock
    ProfileDao profileDao;
    
    @InjectMocks
    private ProfileService profileService;
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newClient();
        this.root = this.client.target(BASEURL);
        //testProfile = new Profile(1L,"biography", "location" ,"website","../avatar.jpg","../image.jpg");
        //profile = new Profile(1L,"biography", "location" ,"website","../avatar.jpg","../image.jpg" );
        for(int i = 1; i <= 10; i++){
            profileList.add(profile);
        }
        profileList.get(0).setId(2L);
    }

    @After
    public void tearDown() {
        profileList = null;
    }    
    
    /**
     * Testing if you can edit a profile
     * If the objects contains the new values than the test will pass else
     * the test will fail
     */
    @Test
    public void editProfileTest() { 
        String mediaType = MediaType.APPLICATION_JSON;        
        when(profileDao.find(2L)).thenReturn(profileList.get(0));
        final Entity<Profile> entity = Entity.entity(profileService.getProfile(2L), mediaType);
        Profile profileResult = this.root.path("edit/2").request().post(entity, Profile.class);
        assertThat(profileResult.getId(), is(profileService.getProfile(2L).getId()));
        //assertThat(profileResult.getUser_id(), is(profileService.getProfile(2L).getUser_id()));
        assertThat(profileResult.getAvatar(), is(profileService.getProfile(2L).getAvatar()));
        assertThat(profileResult.getBiography(), is(profileService.getProfile(2L).getBiography()));
        assertThat(profileResult.getLocation(), is(profileService.getProfile(2L).getLocation()));
        assertThat(profileResult.getWebsite(), is(profileService.getProfile(2L).getWebsite()));
        assertThat(profileResult.getImage(), is(profileService.getProfile(2L).getImage()));
    }
    
    /**
     * If bio got more than 160 characters this test will succeed
     */
    @Test(expected = IllegalArgumentException.class)
    public void editProfileTooManyCharactersBioTest(){       
        when(profileDao.find(1L)).thenReturn(profileList.get(0));
        profileService.getProfile(1L).setBiography("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest");
    }
}
