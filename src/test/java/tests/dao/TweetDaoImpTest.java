package tests.dao;

import dao.TweetDao;
import dao.TweetDaoImp;
import model.Tweet;
import org.junit.Test;
import static org.junit.Assert.*;

public class TweetDaoImpTest {
   
    private final TweetDao tweetDao;
   
    public TweetDaoImpTest() {
        tweetDao = new TweetDaoImp();
    }

    @Test
    public void initTweetsTest() {
        if(tweetDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void createTest() {
        Tweet tweet = new Tweet(1L, "subject11", "some text as content11");
        tweetDao.create(tweet, 1L);
        if(tweetDao.findAll().size() == 11 && tweetDao.find(11L) instanceof Tweet){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void createNullTest() {
        Tweet tweet = null;
        tweetDao.create(tweet, 1L);
        if(tweetDao.findAll().size() == 11 && tweetDao.find(11L) instanceof Tweet){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findAllTest() {
        if(tweetDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findTest() {
        if(tweetDao.find(1L) instanceof Tweet){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void findFailTest() {
        if(tweetDao.find(666L) instanceof Tweet){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }  
    
    @Test
    public void searchTest(){
        if(tweetDao.search("content").size() > 0){
            assertTrue(true);
        }
    }
    
    @Test
    public void searchButNoResultsTest(){
        if(tweetDao.search("waddup").isEmpty()){
            assertTrue(true);
        }
    }
}
