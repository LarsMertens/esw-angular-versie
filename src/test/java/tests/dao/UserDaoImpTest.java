package tests.dao;

import dao.UserDao;
import dao.UserDaoImp;
import java.util.Date;
import model.User;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class UserDaoImpTest {
    
    private final UserDao userDao;
   
    public UserDaoImpTest() {
        userDao = new UserDaoImp();
    }

    @Test
    public void initUsersTest() {
        if(userDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void createTest() {
        User user = new User("username","email@email.com","password","type","firstname","lastname", new Date());
        userDao.create(user);
        if(userDao.findAll().size() == 11 && userDao.find(11L) instanceof User){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void createNullTest() {
        User user = null;
        userDao.create(user);
        if(userDao.findAll().size() == 11 && userDao.find(11L) instanceof User){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findAllTest() {
        if(userDao.findAll().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test
    public void findTest() {
        if(userDao.find(1L) instanceof User){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void findFailTest() {
        if(userDao.find(666L) instanceof User){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }  
    
    @Test
    public void searchTest(){
        if(userDao.search("username1").size() > 0){
            assertTrue(true);
        }
    }
    
    @Test
    public void searchButNoResultsTest(){
        if(userDao.search("jij").isEmpty()){
            assertTrue(true);
        }
    }
}
