package tests.service;

import dao.ProfileDao;
import dao.ProfileDaoImp;
import dao.TweetDao;
import java.util.ArrayList;
import java.util.List;
import model.Profile;
import model.Tweet;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import service.ProfileService;
import service.TweetService;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceTest {
   
    List<Profile> profileList = new ArrayList<Profile>();
    Profile profile;
     
    @Mock
    ProfileDao profileDao;
    
    @InjectMocks
    private ProfileService profileService;
    
    @Before
    public void setUp() {
       //profile = new Profile(1L,"biography", "location" ,"website","../avatar.jpg","../image.jpg" );
        for(int i = 0; i < 10; i++){
            profileList.add(profile);
        }
    }

    @After
    public void tearDown() {
        profileList = null;
    }      

    /**
     * Test if you can create a profile
     * 1 Profile is made if test is succesfull
     */
    @Test
    public void addProfileTest() {
        profileService.addProfile(profile);
        verify(profileDao, Mockito.times(1)).create(profile);
    }
    
    /**
     * Test if you can get all existing Profiles
     * If there are 10 profiles, test will succeed
     */
    @Test
    public void getProfilesTest() {
        when(profileDao.findAll()).thenReturn(profileList);
        assertThat("size is equal to 10", profileService.getProfiles().size(), is(10));
    }
}
