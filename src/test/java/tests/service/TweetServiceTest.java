package tests.service;

import dao.TweetDao;
import dao.UserDao;
import java.util.ArrayList;
import java.util.List;
import model.Tweet;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import service.TweetService;

@RunWith(MockitoJUnitRunner.class)
public class TweetServiceTest {
   
    List<Tweet> tweetList = new ArrayList<Tweet>();
    Tweet tweet;
     
    @Mock
    TweetDao tweetDao;
    
    @InjectMocks
    private TweetService tweetService;
    
    @Before
    public void setUp() {
        tweet = new Tweet(1L, "subject", "content");
        for(int i = 0; i < 10; i++){
            tweetList.add(tweet);
        }
    }

    @After
    public void tearDown() {
        tweetList = null;
    }       

    /**
     * Test if you can create a tweet
     * 1 Tweet is made if test is succesfull
     */
    @Test
    public void addTweetTest() {
        tweetService.addTweet(tweet, 1L);
        verify(tweetDao, Mockito.times(1)).create(tweet, 1L);
    }
        
    /**
     * Test if you can get all existing Tweets
     * If there are 10 tweets, test will succeed
     */
    @Test
    public void getTweetsTest() { 
        when(tweetDao.findAll()).thenReturn(tweetList);
        assertThat("size is equal to 10", tweetService.getTweets().size(), is(10));
    }
    
    /**
     * Test if you can search Tweets for certain content string
     * If 10 results are returned for content the test will succeed
     */
    @Test
    public void searchTweetsTest() {
        when(tweetDao.search("content")).thenReturn(tweetList);
        assertThat("size is equal to 10", tweetService.searchTweets("content").size(), is(10));
    }
}
