package tests.service;

import dao.TweetDao;
import dao.UserDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Tweet;
import model.User;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import service.TweetService;
import service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
   
    List<User> userList = new ArrayList<User>();
    User user;
     
    @Mock
    UserDao userDao;
    
    @InjectMocks
    private UserService userService;
    
    @Before
    public void setUp() {
        user = new User("username","email@email.com","password","type","firstname","lastname",new Date()); 
        for(int i = 0; i < 10; i++){
            userList.add(user);
        }
    }

    @After
    public void tearDown() {
        userList = null;
    }       

    /**
     * Test if you can create an user
     * 1 User is made if test is succesfull
     */
    @Test
    public void addUserTest() {
       userService.addUser(user);
       verify(userDao, Mockito.times(1)).create(user);
    }
    
    /**
     * Test if you can get all existing Users
     * If there are 10 users, test will succeed
     */
    @Test
    public void getUsersTest() {
        when(userDao.findAll()).thenReturn(userList);
        assertThat("size is equal to 10", userService.getUsers().size(), is(10));
    }
    
    /**
     * Test if you can search Users for certain content string
     * If 10 results are returned for username the test will succeed
     */
    @Test
    public void searchUsersTest() {
        when(userDao.search("username")).thenReturn(userList);
        assertThat("size is equal to 10", userService.searchUsers("username").size(), is(10));
    }
}
