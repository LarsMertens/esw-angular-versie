package tests.model;

import dao.ProfileDao;
import dao.ProfileDaoImp;
import java.util.Date;
import model.Profile;
import model.Tweet;
import model.User;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class ProfileTest {
    
    private final ProfileDao profileDao;
    private Profile profile;
   
    public ProfileTest() {
        profileDao = new ProfileDaoImp();
    }
    
    @Before
    public void initObjects() {
        //this.profile = new Profile(1L, "biography", "location", "website", "../avatar.jpg", "../image.jpg");
    }
    
    @After
    public void removeObjects(){
        this.profile = null;
    }
    
    /**
     * Set biography
     */
    @Test
    public void setBiographyTest(){
        this.profile.setBiography("test");
    }
    
    /**
     * Set a biography of more than 160 characters
     */
    @Test(expected = IllegalArgumentException.class)
    public void setTooLongBiographyTest(){
        this.profile.setBiography("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest");
    }
}
