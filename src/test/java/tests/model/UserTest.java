package tests.model;

import dao.ProfileDao;
import dao.ProfileDaoImp;
import dao.TweetDao;
import dao.TweetDaoImp;
import dao.UserDao;
import dao.UserDaoImp;
import java.util.Date;
import model.Tweet;
import model.User;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class UserTest {
    
    private final UserDao userDao;
    private final ProfileDao profileDao;
    private final TweetDao tweetDao;
    private int counter;
    private User user;
    private Tweet tweet;
    
    public UserTest() {
       userDao = new UserDaoImp();
       profileDao = new ProfileDaoImp();
       tweetDao = new TweetDaoImp();
    }
    
    @Before
    public void initObjects() {
        this.user = new User("username","email@email.com","password","type","firstname", "lastname", new Date());  
        this.tweet = new Tweet(1L, "subject", "some text as content 1"); 
    }
    
    @After
    public void removeObjects(){
        this.user = null;
        this.tweet = null;
    }
    
    /**
     * Test if user can login
     */
    @Test
    public void loginTest(){
        assertTrue(this.user.Login("email@email.com", "password"));
    }
    
    /**
     * Test if user can't login with invalid credentials
     */
    @Test
    public void loginInvalidTest(){
        assertFalse(this.user.Login("email@email.com1", "password"));
    }
    
    /**
     * Get 10 latest tweets
     */
    @Test
    public void getLatestTweetsTest(){
        for(int i = 0; i < 20; i++){    
            this.user.getTweets().add(tweet);
        }
        if(this.user.getLatestTweets().size() == 10){
            assertTrue(true);
        } else {
            assertFalse(true);
        }
    }
    
    /**
     * Get an empty list of latest tweest
     */
    @Test
    public void getLatestTweetsIsEmptyTest(){
        assertTrue(this.user.getLatestTweets().isEmpty());
    }
}
