import tests.service.ProfileServiceTest;
import tests.service.TweetServiceTest;
import tests.service.UserServiceTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    ProfileServiceTest.class,
    TweetServiceTest.class,
    UserServiceTest.class,
})

public class TestSuiteService {

}