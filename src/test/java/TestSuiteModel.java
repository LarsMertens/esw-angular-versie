import tests.model.ProfileTest;
import tests.model.TweetTest;
import tests.model.UserTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  ProfileTest.class,
  TweetTest.class,
  UserTest.class,
})

public class TestSuiteModel {

}