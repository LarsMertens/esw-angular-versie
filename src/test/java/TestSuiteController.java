import tests.controller.ProfileControllerTest;
import tests.controller.TweetControllerTest;
import tests.controller.UserControllerTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    ProfileControllerTest.class,
//  TweetControllerTest.class,
//  UserControllerTest.class,
})

public class TestSuiteController {

}